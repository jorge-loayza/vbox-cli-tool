package org.loayza;

import java.util.ArrayList;
import org.virtualbox_6_1.*;

import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(name = "vbox-tool", description = "Command to mange virtialbox vm's")
public class App implements Runnable{
    static {
        // System.setProperty("java.library.path", "lib/vboxjxpcom.jar");
        System.setProperty("vbox.home", "/usr/lib64/virtualbox");
    }

    private static VirtualBoxManager boxManager;
    private static IVirtualBox vbox;

    public static void main(String[] args) throws Exception {
        int exitCode = new CommandLine(new App()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public void run() {
        boxManager = VirtualBoxManager.createInstance(null);
        vbox = boxManager.getVBox();
        // String baseFolder = vbox.getSystemProperties().getDefaultMachineFolder();
        listCommand();
        // createCommand();

    }
    
    @Command(name = "list")
    public void listCommand() {
        try {
            int i = 0;
            for (IMachine m : vbox.getMachines()) {
                System.out.println("Machine " + (i++) + ": " + " [" + m.getId() + "]" + " - " + m.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    @Command(name = "start")
    public void startCommand() {
        // IMachine m = vbox.getMachines().get(0);
        IMachine m = vbox.findMachine("ubuntuServer");
        String name = m.getName();
        System.out.println("\nAttempting to start VM '" + name + "'");

        ISession session = boxManager.getSessionObject();
        ArrayList<String> env = new ArrayList<String>();
        IProgress p = m.launchVMProcess(session, "gui", env);
        progressBar(boxManager, p, 10000);
        session.unlockMachine();
        boxManager.waitForEvents(0);
    }
    
    static boolean progressBar(VirtualBoxManager mgr, IProgress p, long waitMillis) {
        long end = System.currentTimeMillis() + waitMillis;
        while (!p.getCompleted()) {
            // process system event queue
            mgr.waitForEvents(0);
            // wait for completion of the task, but at most 200 msecs
            p.waitForCompletion(200);
            if (System.currentTimeMillis() >= end)
                return false;
        }
        return true;
    }

    @Command(name = "create")
    public void createCommand() {
        IMachine newMachine = vbox.createMachine(null, "test-vm", null, "Ubuntu_64", "");
        newMachine.saveSettings();
        vbox.registerMachine(newMachine);
    }

}
