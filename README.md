# VBOX-CLI-TOOL

Simple Virtualbox cli tool to manage virtual machines.

## Requirements
The following dependencies must be installed and well configured:

1. [Java JDK 11](https://openjdk.java.net/install/)
2. [Maven](https://maven.apache.org/install.html)
## Installation

Open a terminal in the project folder and enter the following commands.

### Install Virtualbox SDK

The following command install the virtualbox sdk into the maven local repository.

```bash
$ mvn install:install-file -Dfile=lib/vboxjxpcom.jar -DgroupId=com.loayza -DartifactId=vboxjxpcom -Dversion=1.0 -Dpackaging=jar -DgeneratePom=true
```

### Install dependencies

To install the required dependencies just enter the following command:

```bash
$ mvn install
```

### Build the project

To build the priject just enter the next command:

```bash
$ mvn clean package
```
## Usage

To use the tool just type the following commad:

```bash
$ java -cp target/vbox-cli-tool-1.0-SNAPSHOT-shaded.jar org.loayza.App list
```